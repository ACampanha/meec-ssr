# Cookie Manipulation

Note: cookie format: hash|username|admin|author (admin)\
		      hash|username||author (normal user)\
		      
1. Create a account with the name:\
hello|admin|author -> the corresponding cookie will be GRUYERE=19...|hello|admin|author||author, o que dará ao utilizador privilégios de administrador do site

## Fix

1. Verificar o formato da cookie -> impedir a criação de uma cookie que tenha algum caracter diferente de letras no user id, impedindo o acesso ao modo de admin a qualquer user\
Modificação na função HandleRequest (CHANGED 8)
2. Foi colocado um prazo de validade para as cookies, com a finalidade de impedir o re-uso da mesma por outros utilizadores ou o mesmo passado 3600s\
Modificado na função _ParseCookie, _createCookie  (CHANGE 8)
