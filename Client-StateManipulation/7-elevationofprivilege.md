# Elevation of Privilege

1. Sign in
2. Edit profile
3. Right click
4. Inspect
5. Insert line'<input type="hidden" name="is_admin" value="True">'
6. Log out and log in again
7. You can now manage the server

## Fix

We edited the _DoSaveprofile function so that the is_admin field can only be edited if the current user is an admin.