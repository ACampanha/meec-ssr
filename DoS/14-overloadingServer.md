# Overloading the Server
1. Create account with username = ../resources e pw= ..
2. Create menubar.gtl with the recursive function:
`[[include:menubar.gtl]]DoS[[/include:menubar.gtl]]`
3. Upload file created in 2. that will replace the file with the same name in the directory resources do gruyere_vulneable
4. Server will crash 

## Fix
- Option 1: Do not allow creation of users with special characters (already implemented in Cookie Manipulation - CHANGE 8)

- Option 2: Escape . and / of the uid to their URL enconding representation (implemented) and removed any / from the filename or username (CHANGE 8 em _isValidUid)

Optional addition to option 2: Generate truly UIDs for each user, instead of using their username. (not implemented)

- Option 3: Only allow write to access the user folder inside "resources" directory and only can read the files inside "resources" directory (CHANGE 11 em _Open) to prevent the user from accessing the other existing files, and modify them and crash the server



