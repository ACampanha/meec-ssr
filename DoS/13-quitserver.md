# Quit of Server
1. Install extension REST Client in VScode

2. (quit_bug)
- Create quit_bug.http with:
http://127.0.0.1:8008/385607992755383036395542385956505108095/quitserver

- Click on send request and it will quit the server, you don't need to be an admin to do this (resposta no file-> 13-quit_server_response_before.html

3. (reset_bug)
- Create reset_bug.http with:
 (Option 1): http://127.0.0.1:8008/385607992755383036395542385956505108095/RESET
 (Option 2): http://127.0.0.1:8008/385607992755383036395542385956505108095/reSET
 (Option 3): ...
 
 - Click on send request and it will reset the server, you don't need to be an admin to do this (resposta no file-> 13-reset_server_response_before.html

## Fix
1. (Fix quit_bug) -> De modo a que apenas o admin pode executar o comando quit

- Modificar na class _GruyereRequestHandler para o seguinte código (CHANGE 13):
```
_PROTECTED_URLS = [
    "/quitserver",
    "/reset"
] 
```

+ Acrescentar código a função _DoQuitServer para verificar qualquer tentativa de execução do comando quit por parte dum user sem ser Admin (CHANGE 13)\
(resposta com bug corrigido no file -> 13-quit_server_response_fixed.html)


2. (Fix reset_bug) -> De modo a que apenas do Admin execute o comando reset server
- (Opcão menos segura) Modificar na função HandleRequest de modo a que coloque todas as letras em minusculas do path, alterar para o seguinte código:
```
  if path.lower() in self._PROTECTED_URLS and not cookie[COOKIE_ADMIN]:
      self._SendError('Invalid request', cookie, specials, params)
      return
```
- (Opção + Segura): Acrescentar código na função _DoReset para verificar qualquer tentativa de execução do comando reset por parte dum user sem ser Admin (CHANGE 13)\
(resposta com bug corrigido no file -> 13-reset_server_response_fixed.html)



 

