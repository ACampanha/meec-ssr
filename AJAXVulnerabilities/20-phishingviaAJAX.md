# Phishing via AJAX
1. Log in
2. Create new snippet with:
```
<a href='https://www.cheesesociety.org/conference/'>Cheese Conference! Registration here!</a>
```
3. Press "Cheese Conference! Registration here!" in the snippet 
4. It will open other web page (https://www.cheesesociety.org/conference/)

## Fix

- Escape html caracteres in the snippet
- Only allow letters in the name 
