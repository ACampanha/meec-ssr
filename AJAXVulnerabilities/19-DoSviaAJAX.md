# DoS via AJAX

1. Log in
2. Create new snippet similar to snippet", private_snippet:"DoS
3. Press refresh
4. Private snippet was replaced with DoS

## Fix

Escape " to its HTML entity encoding representation. That way, it won't interfere with the JSON quotation marks for the value of the snippets.