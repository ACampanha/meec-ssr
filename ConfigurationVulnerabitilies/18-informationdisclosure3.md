# Information Disclosure 3

1. Login
2. Create new snippet with {{_db:pprint}}

## Fix

Escape [] and {} into their HTML entity representation, so they are not interpreted as valid blocks and variables in GTL (Grueyere templating engine)