# STORE XSS
1. Criamos uma nova conta no site; 
2. Várias opçoes de resolução/tentativas
	- (try) criar um snippet com uma mensagem e o seguinte script: `<script>alert(1)</script>` -> é bloqueado pelo site e não executa 
	- utilizamos este scrip no snippet: `<a onclick href='javascript:alert("Hacket")'>You won the lottery!! click here to collect it!</a>` -> clicando no snipp chamado "you won..." aparece a janela de erro
	- (soluçoes) utilizaram este script: `<a onmouseover="alert(1)" href="#">read this!</a>` -> passando o rato em cima deste snippet a mensagem de erro parece no ecra
	
## Fix
O servidor dá escape dos caracteres de HTML, não lendo o script como código mas como texto simples de um snippet.\
Modificamos o código (CHANGE 3)
