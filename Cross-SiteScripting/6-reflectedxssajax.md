# Reflected XSS via AJAX

Inspecting the refresh button on a user's snippet page, we can see that there is an AJAX request to the page:
http://127.0.0.1:8008/.../feed.gtl?uid=`<uid>`

Response example: _feed(( [ "teste" ,"sadasd" ] )) 

The response includes the uid field, and there is no escaping of html tags 
We can exploit this XXS vulnerability by constructing the following URL:

http://127.0.0.1:8008/.../feed.gtl?uid=`<script>alert(document.cookie)</script>`

6. Hack complete

## Fix

Inspecting the code, we found that this is a template response that renders the template `feed.gtl`.

To fix this issue, we edited the _SendTemplateResponse function and applied the htmlescape function previously implemented on the http parameters. This makes every kind of reflected XSS via AJAX attack impossible.
