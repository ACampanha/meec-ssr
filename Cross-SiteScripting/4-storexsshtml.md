# Store XSS via HTML Attribute
(Try)
1. Criar conta
2. Novo Snippet
3. `<b onmouseover= style='color:red'>click me!</b>` -> muda a cor do snippet para "red" quando passamos o rato por cima

(Solução)
1. Criar conta
2. Edit Profile
3. Escrever o seguinte script no campo "Profile Color":`red' onmouseover='alert(document.cookie)`

## Fix
O servidor dá escape dos caracteres especiais de HTML do script colocado no profile color e não executa nada.\
Na modificação de código realizada o servidor dá escape dos caracteres especiais de HTML de possíveis scripts colocados em qualquer campo editável da profile page do utilizador.\
Adicionalmente, criamos a função _SanatizeColor (usada quando é editado o "Profile Color") que aceita apenas o nome da cor (inglês) ou # seguido do código hexadecimal da cor (por exemplo: #FFFFFF), em caso de erro retorna invalid color e utiliza a cor de default (Black).\
Modificamos o codigo em "gruyere.py" (CHANGE 4) - utilizamos a função HTLMescape na _AddParameter.\
(Adicionar dicionário de cores? Para futuro)

