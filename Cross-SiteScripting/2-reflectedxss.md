# Reflected XSS

1. URL bar
2. Go to http://127.0.0.1:8008/.../`<script>alert("Hacking Time")</script>`
3. Hack complete

## Fix
O servidor deve dar escape dos carateres de HTML (como < e >) quando recebe o URL antes de apresentar ao browser.

```
& -> &amp;
< -> &lt;
> -> &gt;
" -> &quot;
' -> &#0x27;
```

Porquê que fazemos do &? O OWASP manda mas não explica