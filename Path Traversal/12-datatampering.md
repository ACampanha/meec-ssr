# Data Tampering via Path Traversal

1. Create user with username ..
2. Sign in on user ..
3. Upload file secret.txt
4. Original file was replaced!

## Fix
Option 1: Do not allow creation of users with special characters (already implemented in Cookie Manipulation)

Option 2: Escape . and / of the uid to their URL enconding representation (implemented) and removed any / from the filename

Optional addition to option 2: Generate truly UIDs for each user, instead of using their username.