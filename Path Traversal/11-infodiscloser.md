# Information disclosure via path traversal
1. Install extension REST Client in VScode
2. Create find_secret.http with:
http://127.0.0.1:8008/385607992755383036395542385956505108095/../secret.txt

Nota: apenas com: http://127.0.0.1:8008//../secret.txt -> termina execução do programa por bad_id

3. Click on send request and it will open a Responde(Xms) with the information of the file (resposta guardada no ficheiro response.html)

## Fix
- Para o fix desta vulnerabilidade impedimos o acesso (visualização e modificação) a determinados ficheiros através dum pedido http ou através do upload de files através do página dum utilizador 

1. Criamos uma lista FORBIDDEN_PATH_NAME com os caracteres proibidos para o path ou para o nome do ficheiro (CHANGE 11)
2. Na função _Open fizemos uma verificação do nome e do path do ficheiro impedindo que determinados ficheiros existentes sejam acedidos ou modificados (CHANGE 11)
3. Acrescentar erro ("You SHALL NOT PASS!!!") nas funções _SendTemplateResponde, _SendFileResponse, _DoUpload2 (CHANGE 11)
4. Acrecentamos parametro de entrada na função _Open (overRideSecurity), para que as funçoes _..Database não tenham de passar pelas medidas de segurança referidas em cima, uma vez que não dependem diretamente do utilizador (CHANGE 11)
