
def HTMLEscape(s):
    s = s.replace("&", "&amp;")
    s = s.replace("<", "&lt;")
    s = s.replace(">", "&gt;")
    s = s.replace("\"", "&quot;")
    s = s.replace("'", "&#x27;")
    s = s.replace("[", "&#91;")
    s = s.replace("]", "&#93;")
    s = s.replace("{", "&#123;")
    s = s.replace("}", "&#125;")
    s = s.replace("\"", "&#34;")
    return s
