import uuid

class SessionManager:
    def __init__(self):
        self._session = {}

    def create_session(self, data = {}):
        session_id = uuid.uuid4().hex
        self._session[session_id] = data
        
        return (session_id, self._session[session_id])

    def is_session_valid(self, session_id):
        return session_id in self._session

    def get_session(self, session_id):
        return self._session[session_id]

    def destroy_session(self, session_id):
        self._session.pop(session_id)