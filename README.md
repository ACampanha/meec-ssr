# Gruyere: Analysis of a Vulnerable Web Application

This repository contains the laboritory setup used to explore and fix the vulnerabilities in [Gruyere](https://google-gruyere.appspot.com/), for the Systemns and Network Security class of M.EEC, FEUP.

## Stucture

There are two versions of the application in this repository, the original (gruyere/vulnerable) and the one with our patches applied (gruyere/fixed). There are also folders containing our notes and exploits for each type of vulnerability.

## Requirements

* docker
* docker-compose

## Usage

Open the terminal and execute the following commands:

```bash
$ cd grueyere
$ docker-compose up
```

Two URLs should appear, one for the vulnerable and one for the fixed versions.

## Credits

### Gruyere

© Google 2017

### Analysis and exploits

* André Campanhã
* Catarina Pereira
* Henrique Rocha
* Mafalda Ferreira