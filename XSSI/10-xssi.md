# XSSI
1. Iniciar sessão
2. Ir profile e criar private snippet
3. Criar xssi.html
4. Abrir xssi.html no browser
5. Aparece o private snippet numa janela alert (xssi_private_snippet.png)

Nota: 
- Apenas se pode criar 1 private snippet
- Caso o user não tenha private snippet ou não se inicie sessão abre uma janela alert vazia (xssi_without_ps.png)

## Fix
(já corrigido pelas seguintas opções de fix:)
- Fix 9 - criação de um token para impedir o acesso à informação privada dum user aos restantes/attacker
- yours JSON response pages should only support POST requests, which prevents the script from being loaded via a script tag.
- Make sure that the script is not executable by appending some non-executable prefix to it, like ])}while(1);</x>. A script running in the same domain can read the contents of the response and strip out the prefix, but scripts running in other domains can't. 

